<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var array $categories ;
 */
?>
<ul>
    <?php foreach ($categories as $category): ?>
        <li>
            <?php if ($category['active']): ?>
               <b><?= $category['label'] ?></b>
            <?php else: ?>
                <a href="<?= Url::to($category['url']) ?>"><?= $category['label'] ?></a>
            <?php endif; ?>
            <?php if (!empty($category['items'])): ?>
                <?= $this->render('topics', [
                    'categories' => $category['items']
                ]) ?>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>